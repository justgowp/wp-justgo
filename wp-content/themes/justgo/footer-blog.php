<div id="rodape">
<?php if ( is_front_page() || is_home() ) { ?>	
	    <div id="rodape_dentro">
        <h1>Fale com a gente!</h1>
        <div id="contato_rodape">
        	<?php echo do_shortcode( '[contact-form-7 id="55" title="Contato1"]' ); ?>
        </div>
        <div id="texto_rodape">
        <p>Preencha o formulário ou envie um e-mail para <a href="mailto:justgo@justgo.blog.br">justgo@justgo.blog.br</a>. Responderemos assim que possível!</p>
        <hr />
        <img src="<?php bloginfo( 'template_url' ); ?>/images/mapa_contato.png" alt>
        </div>
    </div>
<?php } ?>
</div>
<div id="rodapezinho">
<p class="coluna1">Todos os direitos reservados © 2017 Just Go! :) - por Caroline Dias e Leonardo Vieira</p>
<p class="coluna2"><img src="<?php bloginfo( 'template_url' ); ?>/images/xcake.png" alt style="    vertical-align: middle;"> design por <a href="http://www.xcake.com.br/" target="_blank">xCake</a>  +  códigos por <a href="http://www.leonordesigner.com.br/" target="_blank">Leonor Designer</a></p>
</div>
<?php wp_footer(); ?>