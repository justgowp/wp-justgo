<?php /* Template Name: Nos */ ?>
<?php get_header(); ?>
<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>

<div id="conteudo">

<?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

        <div id="parent-<?php the_ID(); ?>" class="parent-page">
			<div class="justgo"><hr />
            	<h2><?php the_title(); ?></h2>
               </div>
			<div class="nos_dentro">
            <p><?php the_content(); ?></p>
			</div>
        </div>

    <?php endwhile; ?>

<?php endif; wp_reset_postdata(); ?>
<div style="clear:both"></div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer(); ?>
</body>
</html>