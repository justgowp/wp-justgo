<div id="slides_index">
    <div class="cycle-slideshow" 
        data-cycle-timeout=4000
        data-cycle-pager=".example-pager"
        data-cycle-slides="> div"
        data-cycle-prev="#prev"
        data-cycle-next="#next"
        >
  
<?php
	$my_query2 = new wp_query( array('posts_per_page' => 4, 'paged' => get_query_var( 'paged' ) ) );
	while( $my_query2->have_posts() ) : $my_query2->the_post(); 
	
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
?>
<div class="slides" style="background-image:url(<?php echo $featured_img_url; ?>);">
    <a rel="external" href="<?php the_permalink()?>">
  <div class="moldura">
  	<div class="dentro">
  	<h1><?php the_title(); ?></h1>
  <div class="justgo"><hr />
                <p>just go!</p></div>
  </div></div>
   </a>
   </div>
<?php
 endwhile; 
$post = $orig_post;
wp_reset_query();
?>        
    </div>
    <div class="example-pager" style="display:none"></div>
    <span id=prev class="cycle-prev"></span>
    <span id=next class="cycle-next"></span>
</div>
