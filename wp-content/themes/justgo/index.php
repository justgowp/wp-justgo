<?php get_header(); ?>

<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>

<?php get_template_part('slides-blog'); ?>

<div id="conteudo">
<?php get_sidebar(); ?>
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div <?php post_class('interna'); ?> id="post-<?php the_ID(); ?>">
            <a href="<?php the_permalink(); ?>">
            	 <?php if ( has_post_thumbnail() ) { ?>
                <div class="imagem_post"><?php the_post_thumbnail('full');  ?></div>
                <?php } ?>
                
                 <div class="categoria">
	    <?php 
 $categories = get_the_category();
 $elemento = array_rand($categories);
if ( ! empty( $categories ) ) {
 //  echo '<a href="' . esc_url( get_category_link( $categories[$elemento]->term_id ) ) . '">' . esc_html( $categories[$elemento]->name ) . '</a>';
      echo esc_html( $categories[$elemento]->name );
} ?>
       </div>
                <h1><?php the_title(); ?></h1>
       <hr class="risco_duplo" />
                <?php the_excerpt(); ?>
                <div class="justgo"><hr />
                <p>just go!</p></div>
              </a> 
            </div>
        <?php endwhile;
		
		if ( function_exists( 'wp_pagination' ) )
    wp_pagination();
		
		 ?>
            
        <?php else : ?>
        
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1>Nada por aqui</h1>
                <p>Por enquanto n&atilde;o h&aacute; postagens com essas especifica&ccedil;&otilde;es.</p>
                <p>Aguarde que logo teremos novidades!</p>
            </div>
        
        <?php endif; ?>
<div style="clear:both"></div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_template_part('porondeandamos'); ?>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<div id="rodape_desktop">
<?php get_footer('blog'); ?>
</div>
<div id="rodape_mobile">
<?php get_footer('mobile'); ?>
</div>
</body>
</html>