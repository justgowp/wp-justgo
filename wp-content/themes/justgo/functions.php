<?php

///////////////////// **************** Cria oespaço para o menu *******************///////////////////

function register_my_menus() {
  register_nav_menus(
    array(
     'menu' => __( 'Menu' ),
	 'porondeandamos' => __( 'Por Onde Andamos' ),
	 'redessociais' => __( 'Redes Sociais' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );



///////////////////// **************** Cria o sidebar *******************///////////////////


function widgets_novos_widgets_init() {
	register_sidebar( array(
		'name'         => __( 'Barra Lateral' ),
		'id'           => 'lateral',
		'description'  => __( 'Aqui vão entrar os widgets do para a Barra Lateral' ),
		'class'         => '', 
		'before_widget' => '<div class="coluna  %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1><span>',
		'after_title'  => '</span></h1>',
	) );
	register_sidebar( array(
		'name'         => __( 'Facebook' ),
		'id'           => 'facebook',
		'class'         => '', 
		'description'  => __( 'Aqui vai entrar o widget do facebook' ),
		'before_widget' => '<div class="coluna  %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h1><span>',
		'after_title'  => '</span></h1>',
	) );	
	register_sidebar( array(
		'name' => 'youtube',
		'id' => 'Youtube',
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '<h1>',
		'after_title' => '</h1>',
	) );
	register_sidebar( array(
		'name' => 'anuncio_home',
		'id' => 'anuncio_home',
		'description'  => __( 'Aqui vai o código do anúncio que entra na home' ),		
		'before_widget' => '',
		'after_widget' => '',
		'before_title' => '',
		'after_title' => '',
	) );		
}

add_action( 'widgets_init', 'widgets_novos_widgets_init' );
		



///////////////////// **************** Cria os novos tamanhos de imagens e altera o thumbnail default *******************///////////////////

add_theme_support( 'post-thumbnails' ); 
add_image_size( 'slide-home', 1090, 410, array( 'center', 'center' ) );
add_image_size( 'thumb-blog', 690, 450, array( 'center', 'center' ) );
add_image_size( 'thumb-sidebar', 290, 190, array( 'center', 'center' ) );
add_image_size( 'justgoplus', 290, 290, array( 'center', 'center' ) );


/////******  Cria imagem destacada atomaticamente -******///////


function odin_autoset_featured() {
    global $post;
    if ( isset( $post->ID ) ) {
        $already_has_thumb = has_post_thumbnail( $post->ID );
        if ( ! $already_has_thumb ) {
            $attached_image = get_children( 'post_parent=' . $post->ID . '&post_type=attachment&post_mime_type=image&numberposts=1' );
            if ( $attached_image ) {
                foreach ( $attached_image as $attachment_id => $attachment ) {
                    set_post_thumbnail( $post->ID, $attachment_id );
                }
            }
        }
    }
}

add_action( 'the_post', 'odin_autoset_featured' );
add_action( 'save_post', 'odin_autoset_featured' );
add_action( 'draft_to_publish', 'odin_autoset_featured' );
add_action( 'new_to_publish', 'odin_autoset_featured' );
add_action( 'pending_to_publish', 'odin_autoset_featured' );
add_action( 'future_to_publish', 'odin_autoset_featured' );

/////******  FIM Cria imagem destacada atomaticamente -******///////



/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    return sprintf( '... <a class="leiamais" href="%1$s">%2$s</a>',
        get_permalink( get_the_ID() ),
        __( 'Just Go!', 'textdomain' )
    );
}
//add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );


/**
 * Filter the except length to 20 characters.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 17;
}
//add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );




/**
 *
 * Paginação utilizando a função paginate_links
 * @param  WP_Query $query Contém uma $query customizada
 *
 */
function wp_pagination( $query=null, $wpcpn_posts=null )
{
    global $wp_query;
    $query = $query ? $query : $wp_query;
    $big = 999999999;
    $max_num_pages = $query->max_num_pages;

    $paginate = paginate_links(
        array(
            'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
            'type'      => 'array',
            'total'     => $max_num_pages,
            'format'    => '?paged=%#%',
            'current'   => max( 1, get_query_var('paged') ),
            'prev_text' => __('&laquo;'),
            'next_text' => __('&raquo;'),
        )
    );
    if ( $max_num_pages > 1 && $paginate ) {
        echo '<ul class="pagination pagination-lg">';
        foreach ( $paginate as $page ) {
            echo '<li>' . $page . '</li>';
        }
        echo '</ul>';
    }
}


?>