<body  <?php body_class( $class ); ?>>
<div id="risco_topo"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_topo.png" alt></div>
<div id="topo">
	<div id="idiomas"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/idiomas.png" alt></a></div>
	<div id="logo"><a href="<?php echo home_url(); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/logo.png" alt></a></div>
    <?php wp_nav_menu( array( 'theme_location' => "redessociais" ) ); ?>
</div>
<div id="risco_branco_topo"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_top.png" alt></div>
<div id="menu_fundo">
	<div id="menu"> 
		<?php wp_nav_menu( array( 'theme_location' => "menu" ) ); ?>
        <div id="busca"><?php get_search_form(); ?></div>
    </div>
	<div id="menu-mobile">
     	<a id="menu-mobile-a" style="display: inline-block;">Menu</a>
		<?php wp_nav_menu( array( 'theme_location' => "menu" ) ); ?>
        <div id="busca"><?php get_search_form(); ?></div>
    </div>    
</div>
<script>
$('#menu-mobile-a').on('click touchstart', function(e){
    $('.menu-menu-1-container').toggleClass('ativo');
    e.preventDefault();
});
$('#menu-mobile #busca .icon').on('click touchstart', function(e){
    $('#menu-mobile #busca').toggleClass('ativo');
    e.preventDefault();
});
</script>