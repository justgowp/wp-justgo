<?php /* Template Name: Just Go Plus */ ?>
<?php get_header(); ?>
<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>

<div id="conteudo">

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>


			<div class="justgo plus">
            	<h2><?php the_title(); ?></h2>
                <img src="<?php bloginfo( 'template_url' ); ?>/images/aviaozinho.png" alt>
               </div>
            <div class="nos_dentro plus" style="text-align:center;">
            <p><?php the_content(); ?></p>
			</div>
            <div class="justgo" style="margin:2em 0"><hr /></div>

    <?php endwhile; ?>

<?php endif; wp_reset_postdata(); ?>
<div style="clear:both"></div>
<div style="margin:0 -7.5%;text-align: center;">
<?php

$args = array(
    'post_type'      => 'page',
    'posts_per_page' => -1,
    'post_parent'    => $post->ID,
    'order'          => 'ASC',
    'orderby'        => 'menu_order'
 );


$parent = new WP_Query( $args );

if ( $parent->have_posts() ) : ?>

    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
     <div <?php post_class('justgoplus'); ?> id="post-<?php the_ID(); ?>">
     <?php if ( has_post_thumbnail() ) { ?>
     <a href="<?php the_permalink(); ?>">
                <?php the_post_thumbnail('justgoplus');  ?>
                <?php } ?>
    
          <p  ><?php the_title(); ?></p>
              </a> 
            </div>
      
        <?php endwhile; endif; ?>
</div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer(); ?>
</body>
</html>