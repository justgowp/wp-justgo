<?php get_header(); ?>
<?php get_template_part('topo'); ?>

<div id="slides_home" style="position: relative;">
    <div class="cycle-slideshow" 
        data-cycle-timeout=4000
        data-cycle-pager=".example-pager"
        data-cycle-slides="> div"
        data-cycle-prev="#prev"
        data-cycle-next="#next"
        >
  
<?php
	$my_query2 = new wp_query( array('posts_per_page' => 4, 'paged' => get_query_var( 'paged' ) ) );
	while( $my_query2->have_posts() ) : $my_query2->the_post(); 
	
	$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
?>
<div class="slides" style="background-image:url(<?php echo $featured_img_url; ?>);">
    <a rel="external" href="<?php the_permalink()?>">
  <span><?php the_title(); ?></span>
   </a>
   </div>
<?php
 endwhile; 
$post = $orig_post;
wp_reset_query();
?>        
    </div>
    <div class="example-pager" style="display:none"></div>
    <span id=prev class="cycle-prev"></span>
    <span id=next class="cycle-next"></span>    
</div>

</div>

<div class="risco_branco_bottom home"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>
<div id="nos_home">
    <div class="nos_home_texto">
        <a href="<?php echo get_page_link(6); ?>">
        <h2 class="azul_italico">nós...</h2>
        <h1 style="font-size:3em;color:#6d6e71;letter-spacing:0.1em">Carol, Léo<br />& Amora</h1>
        <h2 class="azul_italico">just go! :)</h2>
        <p style="font-size:0.9em;font-weight: 300;">Acreditamos que felicidade é colecionar instantes, ser protagonista da sua vida e ter tempo para o que importa! E por este motivo, queremos compartilhar nossas aventuras, então Just Go! :)</p>
        
        </a>
    </div>
    <div style="display:inline-block"><a href="<?php echo get_page_link(6); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/quemsomos_sidebar.jpg" alt="Quem Somos"  /></a></div>
</div>

<div id="posts_home">
	<div id="posts_home_dentro">
    	<div class="coluna um">
            <?php 
            $popularpost = new WP_Query( array( 'posts_per_page' => 2 ) );
            while ( $popularpost->have_posts() ) : $popularpost->the_post();
            ?>
             <a href="<?php the_permalink(); ?>">
                             <?php if ( has_post_thumbnail() ) { ?>
                            <div class="imagem_post"><?php the_post_thumbnail('thumb-sidebar');  ?></div>
                            <?php } ?>
                             <h1><?php the_title(); ?></h1>
                             <div class="justgo"><hr />
                            <p>just go!</p></div>
                            </a>
             <?php
             
            endwhile;
            ?>
         </div>
    	<div class="coluna dois">
            <?php 
            $popularpost = new WP_Query( array( 'posts_per_page' => 1,'offset'=>2 ) );
            while ( $popularpost->have_posts() ) : $popularpost->the_post();
            ?>
             <a href="<?php the_permalink(); ?>">
                             <?php if ( has_post_thumbnail() ) { ?>
                            <div class="imagem_post"><?php the_post_thumbnail('medium');  ?></div>
                            <?php } ?>
                            <div class="categoria">
	    <?php 
 $categories = get_the_category();
 $elemento = array_rand($categories);
if ( ! empty( $categories ) ) {
 //  echo '<a href="' . esc_url( get_category_link( $categories[$elemento]->term_id ) ) . '">' . esc_html( $categories[$elemento]->name ) . '</a>';
      echo esc_html( $categories[$elemento]->name );
} ?>
       </div>
                           <h1><?php the_title(); ?></h1>
       <hr class="risco_duplo" />
                <?php the_excerpt(); ?>
                             <div class="justgo"><hr />
                            <p>just go!</p></div>
                            </a>
             <?php
             
            endwhile;
            ?>
         </div>
    	<div class="coluna tres">
            <?php 
            $popularpost = new WP_Query( array( 'posts_per_page' => 2,'offset'=>3 ) );
            while ( $popularpost->have_posts() ) : $popularpost->the_post();
            ?>
             <a href="<?php the_permalink(); ?>">
                             <?php if ( has_post_thumbnail() ) { ?>
                            <div class="imagem_post"><?php the_post_thumbnail('thumb-sidebar');  ?></div>
                            <?php } ?>
                             <h1><?php the_title(); ?></h1>
                             <div class="justgo"><hr />
                            <p>just go!</p></div>
                            </a>
             <?php
             
            endwhile;
            ?>
         </div>                  
	</div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<div id="vlogs">
	<div id="vlogs_dentro">
    	<h1>assista os últimos vlogs do just go :)</h1>
        <?php if ( dynamic_sidebar('youtube') ) : else : endif; ?>
        <script src="https://apis.google.com/js/platform.js"></script>

<div class="g-ytsubscribe" data-channelid="UCY6XS6pqZCHmPbSy1VqM2LA" data-layout="default" data-count="default"></div>
    </div>
</div>
<?php get_template_part('porondeandamos'); ?>
<div id="anuncie" style="text-align:center;margin:1em 0">
<?php if ( dynamic_sidebar('anuncio_home') ) : else : endif; ?>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer('front-page'); ?>
</body>
</html>