<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta id="viewport" name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
<title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<link href="<?php bloginfo( 'stylesheet_url' ); ?>" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" href='<?php bloginfo('stylesheet_directory'); ?>/favicon.png'/>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://malsup.github.com/jquery.cycle2.js"></script>

<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/jvectormap/jquery-jvectormap-2.0.3.css" type="text/css" media="screen"/>
<script src="<?php bloginfo('stylesheet_directory'); ?>/jvectormap/jquery-jvectormap-2.0.3.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory'); ?>/jvectormap/jquery-jvectormap-world-mill.js"></script>


<?php wp_head(); ?>
</head>