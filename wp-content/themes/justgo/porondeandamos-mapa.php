<?php /* Template Name: Mapa */ ?>
<?php get_header(); ?>
<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>

<div id="conteudo">

<div id="world-map" style="width: 900px; height: 600px;margin:auto"></div>
  <script>
  	var gdpData = {
  "AF": 100,
  "AL": 100,
  "BR": 100
};
  
    $(function(){
		$('#world-map').vectorMap({
		  map: 'world_mill',
		  backgroundColor:'#fff',
		  regionStyle:  {
  initial: {
    fill: '#d1d2d3',
    "fill-opacity": 1,
    stroke: 'none',
    "stroke-width": 0,
    "stroke-opacity": 1
  },
  hover: {
    "fill-opacity": 0.8,
    cursor: 'pointer'
  },
  selected: {
    fill: 'yellow'
  },
  selectedHover: {
  }
},
	  
		  series: {
			regions: [{
			  values: gdpData,
			  scale: ['#C8EEFF', '#15b8d2'],
			  normalizeFunction: 'polynomial'
			}]
		  },
		  onRegionTipShow: function(e, el, code){
			el.html(el.html()+' (GDP - '+gdpData[code]+')');
		  }
		});
    });
  </script>

<div style="clear:both"></div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer(); ?>
</body>
</html>