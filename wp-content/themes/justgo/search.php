<?php get_header(); ?>

<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>
<div id="conteudo">
<?php get_sidebar(); ?>
<div class="interna">
	<h1><?php printf( __( 'Resultados da busca por: %s', 'shape' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
     		
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
           <a href="<?php the_permalink() ?>">
              
                <h1><?php the_title(); ?></h1>
                <?php the_excerpt(); ?>
       </a>
                     
                </div> 
                
                 
</div>
                
                
                                
        <?php endwhile;
		
         
        else : ?>
        
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1>Nada por aqui</h1>
                <p>Por enquanto n&atilde;o h&aacute; postagens com essas especifica&ccedil;&otilde;es.</p>
                <p>Aguarde que logo teremos novidades!</p>
            </div>
        
        <?php endif; ?>

</div>
<div style="clear:both"></div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer(); ?>
</body>
</html>