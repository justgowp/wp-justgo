<div id="rodape">
    <div id="instagram">
    	<h2>instagram</h2>
        <?php echo do_shortcode( '[instagram-feed]' ); ?>
    </div>
    <div id="instagram_mobile">
    	<h2>instagram</h2>
        <?php echo do_shortcode( '[instagram-feed cols=2 imagepadding=3 num=4]' ); ?>
    </div>    
    <hr style="    border-bottom: solid 1px #fff;
    width: 87%;
    margin: 3em auto;" />    
    <div id="siga">
    	<h2>siga a gente<br />
nas redes sociais!</h2>
 <?php wp_nav_menu( array( 'theme_location' => "redessociais" ) ); ?>
    </div>
    <div id="fale_reduzido">
    <h2 style="color:#262262">Fale com a gente!</h2>
    <p>Envie um e-mail para <a href="mailto:justgo@justgo.blog.br">justgo@justgo.blog.br</a></p>
    </div>
</div>
<div id="rodapezinho">
<p class="coluna1">Todos os direitos reservados © 2017 Just Go! :) - por Caroline Dias e Leonardo Vieira</p>
<p class="coluna2"><img src="<?php bloginfo( 'template_url' ); ?>/images/xcake.png" alt style="    vertical-align: middle;"> design por <a href="http://www.xcake.com.br/" target="_blank">xCake</a>  +  códigos por <a href="http://www.leonordesigner.com.br/" target="_blank">Leonor Designer</a></p>
</div>
<?php wp_footer(); ?>