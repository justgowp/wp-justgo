<div id="porondeandamos">
<h1>por onde andamos</h1>
<h2>13 países   •   30 cidades</h2>
<div class="mapa">
	<img src="<?php bloginfo( 'template_url' ); ?>/images/maps.png" alt style="display:none">
<script>
largura=window.screen.width;
altura=window.screen.width * 0.75;
//alert('largura:' + largura + ' altura: ' + altura);
if(largura<768){
	document.write('<div id="world-map" style="width: ' + largura + 'px; height: ' + altura + 'px;margin:auto;overflow:hidden"></div>');
} else {
	document.write('<div id="world-map" style="width: 715px; height: 360px;margin:auto;overflow:hidden"></div>');
}
</script>

<!--<div id="world-map" style="width: 715px; height: 360px;margin:auto;overflow:hidden"></div>-->
  <script>
  	var gdpData = {
  "GB": 100,
  "BR": 100,
  "IE": 100,
  "IT": 100,
  "NL": 100,
  "DK": 100,
  "DE": 100,
  "BE": 100,
  "FR": 100,
  "JP": 100,
  "US": 100
};
  
    $(function(){
		$('#world-map').vectorMap({
		  map: 'world_mill',
		  backgroundColor:'#fff',
		  regionStyle:  {
  initial: {
    fill: '#d1d2d3',
    "fill-opacity": 1,
    stroke: 'none',
    "stroke-width": 0,
    "stroke-opacity": 1
  },
  hover: {
    "fill-opacity": 0.8,
    cursor: 'pointer'
  },
  selected: {
    fill: 'yellow'
  },
  selectedHover: {
  }
},
	  
		  series: {
			regions: [{
			  values: gdpData,
			  scale: ['#C8EEFF', '#15b8d2'],
			  normalizeFunction: 'polynomial'
			}]
		  },
		  onRegionTipShow: function(e, el, code){
		/*	el.html(el.html()+' (GDP - '+gdpData[code]+')');*/
			el.html(el.html());
		  }
		});
    });
  </script>
    
</div>
<?php wp_nav_menu( array( 'theme_location' => "porondeandamos" ) ); ?>
</div>