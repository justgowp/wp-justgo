<?php get_header(); ?>

<?php get_template_part('topo'); ?>
<div class="risco_branco_bottom"><img src="<?php bloginfo( 'template_url' ); ?>/images/bordabranca_bottom.png" alt></div>
<div id="conteudo">
<?php get_sidebar(); ?>
<div class="interna">
     <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
            <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
           
            	 <?php if ( has_post_thumbnail() ) { ?>
                <div class="imagem_post"><?php the_post_thumbnail('full');  ?></div>
                <?php } ?>
                
                 <div class="categoria">
	    <?php 
 $categories = get_the_category();
 $elemento = array_rand($categories);
if ( ! empty( $categories ) ) {
 //  echo '<a href="' . esc_url( get_category_link( $categories[$elemento]->term_id ) ) . '">' . esc_html( $categories[$elemento]->name ) . '</a>';
      echo esc_html( $categories[$elemento]->name );
} ?>
       </div>
                <h1><?php the_title(); ?></h1>
       <div class="data"><?php the_date('d F'); ?><hr /><?php echo get_the_date('Y'); ?></div>
                <?php the_content(); ?>
       
       <?php if(!empty(get_post_meta( get_the_ID(), 'Idioma',false ))){ ?>       
               <ul class="resumo_post">            
<?php

$custom_field_keys = get_post_custom_keys();
foreach ( $custom_field_keys as $key => $value ) {
    $valuet = trim($value);
    if ( '_' == $valuet{0} )
        continue;
    //echo $key . " => " . $value . "<br />";
	$saida = get_post_meta( get_the_ID(), $value , true ); 
	$classe=strtolower(substr($value,0,3)); 	
	echo "<li><span class='" . $classe . "'>" . $value . "</span>: " . $saida . "</li>";
}
?>                  
               </ul>
           <?php } ?> 
             <div class="compartilhe"><hr />
                <ul>
                    <li>compartilhe</li>
                    <li><a href="http://facebook.com/share.php?u=<?php the_permalink() ?>&amp;t=<?php echo urlencode(the_title('','', false)) ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook"><img src="<?php bloginfo( 'template_url' ); ?>/images/compartilhe_face.png" /></a></li>
                    <li><a href="http://twitter.com/intent/tweet?text=<?php the_title();?>&url=<?php the_permalink();?>" title="Twittar sobre <?php the_title();?>" target="_blank"><img src="<?php bloginfo( 'template_url' ); ?>/images/compartilhe_twitter.png" /></a></li>
                    <li><a target="_blank" href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&description=<?php the_title(); ?>"><img src="<?php bloginfo( 'template_url' ); ?>/images/compartilhe_pinterest.png" /></a></li>
				</ul>
                </div>
                
                <div class="tags"><?php the_tags( '', '&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;', '<br />' ); ?></div>
                
                </div> 
                
                  <div id="vejatambem">
        <h1><span>Veja Também</span></h1>
        <?php 
//$popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
$popularpost = new WP_Query( array( 'posts_per_page' => 2 ) );
while ( $popularpost->have_posts() ) : $popularpost->the_post();
?>
 <a href="<?php the_permalink(); ?>" class="coluna">
            	 <?php if ( has_post_thumbnail() ) { ?>
                <div class="imagem_post"><?php the_post_thumbnail('thumb-sidebar');  ?></div>
                <?php } ?>
                 <h1><?php the_title(); ?></h1>
                 <div class="justgo"><hr />
                <p>just go!</p></div>
                </a>
 <?php
 
endwhile;
?>
</div>
                
                
                
                	<div id="comentarios">
    <?php comments_template('/comentarios.php'); ?>
    </div>
    
    <div id="porondeandamos_single_mobile">
    <?php get_template_part('porondeandamos'); ?>
    </div>
                
        <?php endwhile;
		
		if ( function_exists( 'wp_pagination' ) )
    wp_pagination();
		
		 ?>
            
        <?php else : ?>
        
            <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1>Nada por aqui</h1>
                <p>Por enquanto n&atilde;o h&aacute; postagens com essas especifica&ccedil;&otilde;es.</p>
                <p>Aguarde que logo teremos novidades!</p>
            </div>
        
        <?php endif; ?>
        
      

</div>
<div style="clear:both"></div>
</div>
<div class="risco_meio"><img src="<?php bloginfo( 'template_url' ); ?>/images/risco_meio.png" alt></div>
<?php get_footer(); ?>
</body>
</html>