<div id="sidebar">
<a href="<?php echo get_page_link(6); ?>">
<h2 class="azul_italico">nós...</h2>
<h1>Carol, Léo<br />& Amora</h1>
<p><img src="<?php bloginfo( 'template_url' ); ?>/images/quemsomos_sidebar.jpg" alt="Quem Somos"  /></p>
<p>Acreditamos que felicidade é colecionar instantes, ser protagonista da sua vida e ter tempo para o que importa! E por este motivo, queremos compartilhar nossas aventuras, então...</p>
<p>&nbsp;</p>
<h2 class="azul_italico">just go! :)</h2>
</a>
<hr />
<?php if ( dynamic_sidebar('facebook') ) : else : endif; ?>
<div class="coluna instagram">
<h1><span><a href="https://www.instagram.com/justgo_photos/" target="_blank">instagram</a></span></h1>
<?php echo do_shortcode('[instagram-feed cols=2 imagepadding=3 num=4]'); ?>
</div>
<?php if ( dynamic_sidebar('lateral') ) : else : endif; ?>
<div class="coluna postsmaislidos">
<h1><span>posts mais lidos</span></h1>
<?php 
//$popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
$popularpost = new WP_Query( array( 'posts_per_page' => 4 ) );
while ( $popularpost->have_posts() ) : $popularpost->the_post();
?>
 <a href="<?php the_permalink(); ?>">
            	 <?php if ( has_post_thumbnail() ) { ?>
                <div class="imagem_post"><?php the_post_thumbnail('thumb-sidebar');  ?></div>
                <?php } ?>
                 <h1><?php the_title(); ?></h1>
                 <div class="justgo"><hr />
                <p>just go!</p></div>
                </a>
 <?php endwhile; ?>
</div>

<?php if (is_single()){ ?>
	
    <div id="porondeandamos">
<h1>por onde andamos</h1>
<h2>13 países   •   30 cidades</h2>
<div class="mapa">
	<img src="<?php bloginfo( 'template_url' ); ?>/images/maps.png" alt>
    <?php //echo do_shortcode('[freehtml5map id="0"]'); ?>
</div>
</div>
    
    <?php
}

?>


</div>

